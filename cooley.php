<!DOCTYPE html> 
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>University Libraries</title>
	<link  href="common/css/ColgateMobile.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="common/js/jquery-1.6.4.js"></script>
	<script type="text/javascript" src="common/js/jquery.mobile-1.0rc1.js"></script>
<?php
$QS_Value = $_SERVER['QUERY_STRING'];
parse_str($QS_Value);
if ($native === '1') {
	echo "<style type='text/css'> #nativenav {display: block;}</style>";
	echo "<style type='text/css'> #mobilenav {display: none;}</style>";
	echo "<style type='text/css'> #nativefooter {display: block;}</style>";
	echo "<style type='text/css'> #mobilefooter {display: none;}</style>";
}
else {
	echo "<style type='text/css'> #nativenav {display: none;}</style>";
	echo "<style type='text/css'> #mobilenav {display: block;}</style>";
	echo "<style type='text/css'> #nativefooter {display: none;}</style>";
	echo "<style type='text/css'> #mobilefooter {display: block;}</style>";
}
?> 
<meta name="format-detection" content="telephone=yes">
</head> 
<body> 
<div data-role="page" id="home" data-title="University Libraries">
    <div data-role="header" data-position="fixed">
        <div id="mobilenav" class="pagetitle">
            <div style="float:left"><a href="https://m.colgate.edu/home"><img src="images/homelink4.png" width="57" height="44" alt="Colgate Mobile Web Home"></a></div>
            <div class="headertext" style="float:left"><a href="index.php"><img class="moduleicon" src="images/webcams_icon.png" width="30" height="30" alt="Library Home"></a></div> 
            <div class="headertext" style="float:left">Cooley Science Library</div>
            <div style="float:right"><a href="help.php"><img src="images/help.png" width="46" height="44" alt="Help"></a></div>
        </div>
        <div id="nativenav" style="display:none;">
            <div style="float:left; background-color:#fff; padding:5px; width:100%"><a href="webcamhelp.php?native=<?php echo $native?>" style="color:#000; font-size:14px; text-decoration:underline;">Info</a></div> 
        </div>
    </div>
	<div data-role="content">


            <h3>Cooley Science Library Hours</h3>

            <ul data-inset="true" data-role="listview">
                <li><a href="https://m.colgate.edu/facility_hours/detail?groupID=libraries&id=id1&_b=%5B%7B%22t%22%3A%22Facility+Hours%22%2C%22lt%22%3A%22Facility+Hours%22%2C%22p%22%3A%22index%22%2C%22a%22%3A%22%22%7D%5D" rel="external">Hours</a></li></ul>

            
            <ul data-inset="true" data-role="listview">
                <li><a href="tel:3152287312" rel="external">Cooley Science
                    (315) 228-7312</a>
                </li>
            </ul>
	</div>
	<div data-role="footer" style="margin-left:10px;" id="mobilefooter">
    	<div style="margin-top:30px;"><a href="#top">Back to top</a> | <a href="https://m.colgate.edu/home">Colgate Mobile home</a></div>
		<div style="margin-top:20px;"><a href="http://www.colgate.edu">&#169; 2013 Colgate University</a></div>
	</div>
	<div data-role="footer" style="margin-left:10px;" id="nativefooter">
		<div style="margin-top:20px;"><a href="http://www.colgate.edu">&#169; 2013 Colgate University</a></div>
	</div>
</div>
</body>
</html>
